const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const browserSync = require('browser-sync').create();
const rename = require("gulp-rename");
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const scssSrc = 'app/app.scss';
const jsFiles = 'app/**/*.js';

gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: '.'
        }
    })
});

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('scss', function(){
    return gulp.src(scssSrc)
        .pipe(sass())
        .pipe(cssnano())
        .pipe(autoprefixer({
            browsers: ['>1%'],
            add: true
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('watch', ['browserSync', 'scss', 'scripts'], function(){
    gulp.watch('app/**/*.scss', ['scss']);
    gulp.watch('app/**/*.js', ['scripts']);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/**/*.js', browserSync.reload);
});