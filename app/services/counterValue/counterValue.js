(function () {

    'use strict';

    function counterValue () {
        var val;
        return {
            getFinalValue : function() {
                return parseFloat(val);
            },
            setFinalValue : function(input) {
                val = parseFloat(input);
            }
        }
    }

    counterValue.$inject = [];

    angular
        .module('gog.counterValue', [])
        .factory('counterValue', counterValue);

})();
