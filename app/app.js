'use strict';


angular.module('gog', [
    'ngRoute',
    'angular-svg-round-progressbar',
    'rzModule',
    /* ---- VIEWS ---- */
    'gog.promo',
    /* ---- COMPONENTS ---- */
    'gog.price-slider',
    'gog.sales-counter',
    /* ---- DIRECTIVES ---- */
    'gog.input-number',
    /* ---- SERVICES ---- */
    'gog.counterValue'
]).

config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {

  $routeProvider
      .when('/promo', {
        templateUrl: 'app/views/promo/promo.html',
        controller: 'PromotionController',
        controllerAs: 'promoCtrl'
      })
      .otherwise({redirectTo: '/promo'});
}]);
