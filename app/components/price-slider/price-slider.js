(function () {

    'use strict';

    function PriceSliderCtrl () {
        var vm = this;
        vm.minVal = 0.99;
        vm.maxVal = 49.99;
        vm.pricePoint_1 = 7.67;
        vm.pricePoint_2 = 18.31;
        vm.slider = {
            value: 0.99,
            min: vm.minVal,
            max: vm.maxVal,
            options: {
                showTicks: true,
                ticksArray: [vm.pricePoint_1, vm.pricePoint_2],
                showTicksValues: true,
                step: 0.1,
                precision: 2,
                floor: 0.99,
                ceil: 49.99,
                enforceStep: false,
                translate: function(value) {
                    return '$' + value;
                },
                onChange: function() {
                    var sliderWidth = document.querySelector('.rz-bar').offsetWidth;
                    var handleWidth =  parseInt(document.querySelector('.rzslider .rz-pointer').offsetWidth);
                    var handleOffset = parseInt(document.querySelector('.rzslider .rz-pointer').style.left);
                    document.querySelector('.input-number__wrp').style.left = (handleOffset-30).toString() + "px";
                    var bgPos = sliderWidth - handleOffset - handleWidth/2;
                    document.querySelector('.rz-bar').style.backgroundPosition = "-" + bgPos.toString() + "px bottom";
                }
            }
        };

        vm.promoData = [
            {
                imgSrcActive: '/app/assets/img/divinity1-active.png',
                imgSrcInactive: '/app/assets/img/divinity1-inactive.png',
                title: 'Divine Divinity',
                priceNormal: '(normal price $5.99)',
                bonus: 'with 6 goodies and 4 language versions',
                status: 'Below average',
                pricePoint: null
            },
            {
                imgSrcActive: '/app/assets/img/divinity2-active.png',
                imgSrcInactive: '/app/assets/img/divinity2-inactive.png',
                title: 'Beyond Divinity',
                priceNormal: '(normal price $5.99)',
                bonus: 'with 6 goodies and 4 language versions',
                status: 'Above average',
                pricePoint: vm.pricePoint_1
            },
            {
                imgSrcActive: '/app/assets/img/divinity3-active.png',
                imgSrcInactive: '/app/assets/img/divinity3-inactive.png',
                title: 'Divinity 2',
                priceNormal: '(preorder, normal price $19.99)',
                bonus: 'with 6 goodies and 4 language versions',
                status: 'Top supporter',
                pricePoint: vm.pricePoint_2
            }
        ];

        vm.changeImg = function(index) {
          if (vm.slider.value < vm.promoData[index].pricePoint) {
              return vm.promoData[index].imgSrcInactive;
          } else {
              return vm.promoData[index].imgSrcActive;
          }
        };

        vm.changeIco = function(index) {
          if (vm.slider.value < vm.promoData[index].pricePoint) {
              return '/app/assets/img/ico-unchecked.png';
          }  else {
              return 'app/assets/img/ico-checked.png';
          }
        };

        vm.getFinalVal = function() {
            return parseFloat(vm.slider.value);
        };

    }

    PriceSliderCtrl.$inject = [];

    angular
        .module('gog.price-slider', [])
        .component('priceSlider', {
            templateUrl: 'app/components/price-slider/price-slider.html',
            controller: PriceSliderCtrl,
            controllerAs: 'productCtrl'
        });
})();