(function () {
    'use strict';

    function SalesCounterController ($timeout) {
        var vm = this;
        var timer;

        vm.counter = 0;
        vm.isHidden = true;

        vm.stopCounter = function() {
            $timeout.cancel(timer);
            timer = null;
        };

        vm.startCounter = function() {
            if (timer === null) {
                vm.updateCounter();
            }
        };

        vm.updateCounter = function() {
            vm.counter++;                                       // COUNTER PLACE VALUES :
            vm.hts = Math.floor(vm.counter / 100000 % 10);    // hundred thousands
            vm.tts = Math.floor(vm.counter / 10000 % 10);     // ten thousands
            vm.ts  = Math.floor(vm.counter / 1000 % 10);      // thousands
            vm.hs  = Math.floor(vm.counter / 100 % 10);       // hundreds
            vm.t   = Math.floor(vm.counter / 10 % 10);        // tens
            vm.o   = Math.floor(vm.counter % 10);             // ones
            timer = $timeout(vm.updateCounter, 0.1);
        };

        vm.updateCounter();

        /* TABS */

        vm.currentTab = 0;

        vm.counterProgress = function(index) {
            var val = vm.counter / vm.tabs[index].title * 100
            if (val > 100) {
                return 100;
            } else {
                return val;
            }
        };

        vm.setTab = function(tabId) {
            vm.currentTab = tabId;
        };

        vm.isSet = function(tabId) {
            return vm.currentTab === tabId;
        };

        vm.setNext = function() {
            if (vm.currentTab + 1 >= vm.tabs.length) {
                vm.currentTab = 0;
            } else {
                vm.setTab(vm.currentTab + 1);
            }
        };

        vm.setPrev = function() {
            if (vm.currentTab - 1 <= 0) {
                vm.currentTab = vm.tabs.length - 1;
            } else {
                vm.setTab(vm.currentTab - 1);
            }
        };

        vm.tabs = [
            {
                title: 10000,
                caption: '...to unlock exclusive, never before seen, trailer from Divinity: Original Sin.',
                videoUrl: 'app/assets/img/thumbnail.png'
            },
            {
                title: 25000,
                caption: '...to unlock exclusive, never before seen, trailer from Divinity: Original Sin. Even more.',
                videoUrl: 'app/assets/img/thumbnail.png'
            },
            {
                title: 50000,
                caption: '...to unlock exclusive, never before seen, trailer from Divinity: Original Sin. Even moooore.',
                videoUrl: 'app/assets/img/thumbnail.png'
            },
            {
                title: 80000,
                caption: '...to unlock exclusive, never before seen, trailer from Divinity: Original Sin. Even moooaar.',
                videoUrl: 'app/assets/img/thumbnail.png'
            },
            {
                title: 120000,
                caption: '...to unlock exclusive, never before seen, trailer from Divinity: Original Sin. The mooooarest',
                videoUrl: 'app/assets/img/thumbnail.png'
            }
        ];
    }

    SalesCounterController.$inject = ['$timeout'];

    angular
        .module('gog.sales-counter', [])
        .component('salesCounter', {
            templateUrl: 'app/components/sales-counter/sales-counter.html',
            controller: SalesCounterController,
            controllerAs: 'counterCtrl'
        });
})();