(function () {
    'use strict';

    function PromotionController () {
        var self = this;

        self.features = [
            {
                icoSrc: '/app/assets/img/ico-support.png',
                caption: 'Support Larian Studios'
            },
            {
                icoSrc: '/app/assets/img/ico-mouse.png',
                caption: 'Play Divinity 2:DC before release'
            },
            {
                icoSrc: '/app/assets/img/ico-drm.png',
                caption: 'Get DRM-free games with goodies'
            },
            {
                icoSrc: '/app/assets/img/ico-time.png',
                caption: 'Only 23:54:55 left'
            }
        ];

        self.goodies = [
            {
                icon: 'app/assets/img/ico-ost.png',
                title: '4 soundtracks',
                caption: 'Over 3 hours of award winning music from all 3 games.'
            },
            {
                icon: 'app/assets/img/ico-stories.png',
                title: '2 short stories',
                caption: 'Prequel story for Divinie Divinity and Beyond Divinity novella.'
            },
            {
                icon: 'app/assets/img/ico-journal.png',
                title: 'Divinity 2 - Dev Journal',
                caption: '144 pages long book, detailing story and art of Divinity 2.'
            },
            {
                icon: 'app/assets/img/ico-film.png',
                title: 'Making of Divinity 2',
                caption: '40 minutes long, professional documentary about the development od Divinity 2.'
            },
            {
                icon: 'app/assets/img/ico-mac.png',
                title: '7 wallpapers',
                caption: 'Beautiful, hand crafted HD wallpapers with Divine, Beyond and Divinity 2 art. '
            },
            {
                icon: 'app/assets/img/ico-more.png',
                title: '...and more',
                caption: '3 manuals, 56 artworks, 5 avatars, Beyond Divinity game-guide.'
            }
        ];
    }

    PromotionController.$inject = [];

    angular
        .module('gog.promo', [])
        .controller('PromotionController', PromotionController);
})();